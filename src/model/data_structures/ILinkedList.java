package model.data_structures;

/**
 * Abstract Data Type for a linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd,	2 AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
//// Agregar al principio.
public interface ILinkedList<T> extends Iterable<T> 
{
	/**
	 * Retorna el tama�o de la lista encadenada
	 * @return tama�o de la lista encadenada
	 */
	public Integer getSize();
	/**
	 * A�ade un elemento a la lista al principio de la lista.
	 */
	public void add(T elemento);
	/**
	 * A�ade un elemento a lo ultimo de la lista.
	 */
	public void addAtEnd(T elemento);
	/**
	 * A�ade un elemento en una posicion
	 */
	public void AddAtK(T elemento, int posicion);
	/**
	 * Retorna el elemento actual.
	 */
	public T getCurrentElement();
	/**
	 * Busca un elemento en la lista y lo retorna.
	 */
	public T getElement(T dato);
	/**
	 * Elimina un elemento de la lista
	 */
    public void delete(T eliminado);
    /**
	 * Elimina un elemento en una posicion
	 */
    public void deleteAtk(int posicion);
    /**
     * Retorna el siguiente nodo de la lista.
     */
    public Node<T> next();
    /**
     * Retorna el anterior nodo de la lista.
     */
    public Node<T> previous();
    /**
     * Retorna si la lista esta vacia;
     */
    public boolean isEmpty();
}
