package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;
/**
 * Clase que representa un iterador 
 * @author nicot
 * @param <T>
 */
public class IteratorLista<T> implements Iterator<T>
{
	/**
	 * Nodo que representa al proximo del actual
	 */
	private Node<T> proximo;
	/**
	 * Nodo que representa al actual (anterior del proximo)
	 */
	private Node<T> anterior;
	/**
	 * Nodo que representa al anterior del actual.
	 */
	private Node<T> ant_ant;

	/**
	 * Construye el iterador y asigna 
	 * @param primero
	 */
	public IteratorLista(Node<T> primero) 
	{
		// TODO Auto-generated constructor stub
		proximo=primero;
		anterior=null;
		ant_ant=null;
	}

	@Override
	public boolean hasNext() 
	{
		// TODO Auto-generated method stub
		return proximo!=null;
	}

	@Override
	public T next() 
	{
		// TODO Auto-generated method stub
		if ( proximo == null )
		{ 
			throw new NoSuchElementException(); 
		}
		T elemento = proximo.getElemento(); // ultimo elemento visitado
		ant_ant=anterior;
		anterior=proximo;
		proximo = proximo.getSiguiente(); // proximo nodo a visitar
		return elemento;
	}
	/**
	 * Retorna el nodo actual (o el anterior al proximo)
	 * @return
	 */
	public Node<T> getAnterior()
	{
		return anterior;
	}
	/**
	 * Retorna el nodo anterior al actual.(O el anterior del anterior)
	 * @return
	 */
	public Node<T> getAnt_Ant()
	{
		return ant_ant;
	}
	/**
	 * Retorna el nodo proximo.
	 * @return
	 */
	public Node<T> getProximo()
	{
		return proximo;
	}
	
}
