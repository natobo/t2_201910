package model.data_structures;

/**
 * Clase que representa una la lista 
 * @author nicot
 * @param <T> tipo generico de la lista
 */
public class LinkedList<T> implements ILinkedList<T>
{
	/**
	 *tama�o de la lista .
	 */
	private int tama�o;
	/**
	 * Primer nodo de la lista. 
	 */
	private Node<T> primero;
	/**
	 * Ultimo nodo de la lista. 
	 */
	private Node<T> cola;
	/**
	 * Nodo actual de la lista;
	 */
    private Node<T> actual;
	/**
	 * Constructor de la lista.
	 */
	public LinkedList() 
	{
		primero=null;
		cola=null;
		actual=primero;
		tama�o=0;
	}

	@Override
	public IteratorLista<T> iterator() 
	{
		// TODO Auto-generated method stub.
		return new IteratorLista<T>(primero);
	}
	@Override
	public Integer getSize() 
	{
		// TODO Auto-generated method stub
		return tama�o;
	}
	@Override
	public void add(T elemento) 
	{
		// TODO Auto-generated method stub
		Node<T> nodoNuevo=new Node<T>(elemento,primero);
		if(primero==null)
		{
			cola=nodoNuevo;
		}
		primero=nodoNuevo;
		actual=primero;
		tama�o++;	
	}
	@Override
	public void addAtEnd(T elemento) 
	{
		// TODO Auto-generated method stub
		Node<T> nodoElemento=new Node<T>(elemento);
		if(cola==null)
		{
			primero=nodoElemento;
			actual=nodoElemento;
			cola=nodoElemento;
		}
		else
		{
			cola.setSiguiente(nodoElemento);
			cola=nodoElemento;
		}
		tama�o++;
	}
	@Override
	public void AddAtK (T elemento, int posicion ) 
	{
		Node<T> nodoElemento=new Node<T>(elemento);
		if(posicion==1)
		{
			nodoElemento.setSiguiente(primero);
			primero=nodoElemento;
			actual=nodoElemento;
		}
		else
		{
			IteratorLista<T> it=iterator();
			int cont=0;
			while(it.hasNext()&&cont!=posicion)
			{
				it.next();
				cont++;
			}
			Node<T> nodoAnterior=it.getAnterior();
			Node<T> nodoAnt_Ant=it.getAnt_Ant();
			nodoElemento.setSiguiente(nodoAnterior);
			nodoAnt_Ant.setSiguiente(nodoElemento);
		}
		tama�o++;
	}

	@Override
	public T getCurrentElement() 
	{
		// TODO Auto-generated method stub
	    return actual.getElemento();
	}

	@Override
	public T getElement(T dato) 
	{
		// TODO Auto-generated method stub
		T elemento=null;
		T tmp=null;
		IteratorLista<T> it=iterator();
		boolean loEncontro=false;
		while(it.hasNext()&&!loEncontro)
		{
			tmp=it.next();
			if(tmp.equals(dato))
			{
				loEncontro=true;
				elemento=tmp;
			}
		}
		return elemento;
	}



	@Override
	public void delete(T eliminado) 
	{
		// TODO Auto-generated method stub

		if(primero.getElemento().equals(eliminado))
		{
			primero=primero.getSiguiente();
			actual=primero;
			tama�o--;	
		}
		else
		{
			IteratorLista<T> it=iterator();
			T elemento=null;
			while(it.hasNext())
			{
				elemento=it.next();
				if(eliminado.equals(elemento))
				{
					Node<T> nodoAnt_Ant=it.getAnt_Ant();
					Node<T> nodoProximo=it.getProximo();
					nodoAnt_Ant.setSiguiente(nodoProximo);
					tama�o--;
					if(eliminado.equals(cola.getElemento()))
					{
						cola=nodoAnt_Ant;
					}
				}
			}	
		}
	}


	@Override
	public Node<T> next()  
	{ 		
		return (actual.getSiguiente()!=null)?actual.getSiguiente():null;
	}
	
	
	@Override
	public Node<T> previous()  
	{ 
	  IteratorLista<T> it=iterator();
	  Node<T> anterior=primero;
	  while(it.hasNext()&&anterior.getSiguiente()!=actual)
	  {
		  it.next();
		  anterior=it.getAnterior();
	  }
	  return anterior;
	  
	}


	@Override
	public void deleteAtk(int posicion) 
	{
		// TODO Auto-generated method stub
		if(posicion==1)
		{
			primero=primero.getSiguiente();
			actual=primero;
			tama�o--;	
		}
		else
		{
			IteratorLista<T> it=iterator();
			int cont=0;
			while(it.hasNext()&&cont!=posicion)
			{
				it.next();
				cont++;
			}
			Node<T> nodoProximo=it.getProximo();
			Node<T> nodoAnt_Ant=it.getAnt_Ant();
			nodoAnt_Ant.setSiguiente(nodoProximo);
			if(cont==tama�o)
			{
				cola=nodoAnt_Ant;
			}
			tama�o--;	
		}
	}

	@Override
	public boolean isEmpty() 
	{
		// TODO Auto-generated method stub
		return (primero==null)?true:false;
	}
	/**
	 * Retorna la cabeza de la lista
	 * @return primero
	 */
	public Node<T> getPrimero()
	{
		return primero;
	}
	/**
	 * Retorna la Cola de la lista
	 * @return primero
	 */
	public Node<T> getCola()
	{
		return cola;
	}
}