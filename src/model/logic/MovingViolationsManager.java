package model.logic;

import java.io.File;
import java.io.FileReader;
import java.util.Iterator;

import com.opencsv.CSVReader;
import api.IMovingViolationsManager;
import javafx.geometry.VPos;
import model.vo.VOMovingViolations;
import model.data_structures.LinkedList;

public class MovingViolationsManager implements IMovingViolationsManager 
{

	/**
	 * Lista general de infracciones.
	 */
	public LinkedList<VOMovingViolations> listaInfrac=new LinkedList<VOMovingViolations>();
	
	/**
	 * Carga todas las infracciones
	 */
	public void loadMovingViolations(String movingViolationsFile)
	{
		// TODO Auto-generated method stub
		try
		{
			CSVReader csvReader = new CSVReader(new FileReader(movingViolationsFile));
			String[] fila=null;
			csvReader.readNext();
			while ((fila=csvReader.readNext()) != null) 
			{
				String objectId=fila[0];
				String violationCode=fila[14];
				String violationDescrip=fila[15];
				String location= fila[2];
				String totalPaid=fila[9];
				String accidentIndicator=fila[12];
				String ticketIssueDate= fila[13];
				
				//Crea la infraccion.
				
				VOMovingViolations infraccion=new VOMovingViolations(objectId, violationDescrip, location, totalPaid, accidentIndicator, ticketIssueDate, violationCode);
				
				//La agrega a la lista
				
				listaInfrac.add(infraccion);
			}
             csvReader.close();
		}
		catch (Exception e) 
		{
			// TODO: handle exception
			e.printStackTrace();
		}
	}


	@Override
	public LinkedList <VOMovingViolations> getMovingViolationsByViolationCode (String violationCode) 
	{
		// TODO Auto-generated method stub
		Iterator<VOMovingViolations> it=listaInfrac.iterator();
		
		LinkedList<VOMovingViolations> rta=new LinkedList<VOMovingViolations>();
		
		while(it.hasNext())
		{
			VOMovingViolations tmp=it.next();
			
			if(tmp.getViolationCode().equals(violationCode))
			{
				rta.add(tmp);
			}
		}
		
		return rta;
	}

	@Override
	public LinkedList <VOMovingViolations> getMovingViolationsByAccident(String accidentIndicator) 
	{
		// TODO Auto-generated method stubIterator<VOMovingViolations> it=listaInfrac.iterator();
		Iterator<VOMovingViolations> it=listaInfrac.iterator();
		
		LinkedList<VOMovingViolations> rta=new LinkedList<VOMovingViolations>();
		
		while(it.hasNext())
		{
			VOMovingViolations tmp=it.next();
			
			if(tmp.getAccidentIndicator().equals(accidentIndicator))
			{
				rta.add(tmp);
			}
		}
		
		return rta;
	}	


}
