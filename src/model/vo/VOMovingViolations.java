package model.vo;

/**
 * Representation of a Trip object
 */
public class VOMovingViolations
{
   /**
    * objectId del auto
    */
   private String objectId;
   /**
    * violationDescription del auto
    */
   private String violationDescription;
   /**
    * location del auto
    */
   private String location;
   /**
    * totalPaid del auto
    */
   private String totalPaid;
   /**
    * accidentIndicator del auto
    */
   private String accidentIndicator;
   /**
    * ticketIssueDate del auto
    */
   private String ticketIssueDate;
   /**
    * violationCode del auto
    */
	private String violationCode;
	
	/**
	 * constructor
	 */
	public VOMovingViolations(String pObjectId,String pViolationDescription,String pLocation,String pTotalPaid,String pAccidentIndicator, String pTicketIssueDate,String pViolationCode)
	{ 
		objectId=pObjectId;
		violationDescription=pViolationDescription;
		location=pLocation;
		totalPaid=pTotalPaid;
		accidentIndicator=pAccidentIndicator;
		ticketIssueDate=pTicketIssueDate;
		violationCode=pViolationCode;
	
	}
	/**
	 * @return id - Identificador único de la infracción
	 */
	public int objectId() 
	{
		// TODO Auto-generated method stub
		return Integer.parseInt(objectId);
	}	
	
	
	/**
	 * @return location - Dirección en formato de texto.
	 */
	public String getLocation() {
		// TODO Auto-generated method stub
		return location;
	}

	/**
	 * @return date - Fecha cuando se puso la infracción .
	 */
	public String getTicketIssueDate() {
		// TODO Auto-generated method stub
		return ticketIssueDate;
	}
	
	/**
	 * @return totalPaid - Cuanto dinero efectivamente pagó el que recibió la infracción en USD.
	 */
	public int getTotalPaid() {
		// TODO Auto-generated method stub
		return Integer.parseInt(totalPaid);
	}
	
	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() {
		// TODO Auto-generated method stub
		return accidentIndicator;
	}
		
	/**
	 * @return description - Descripción textual de la infracción.
	 */
	public String  getViolationDescription() {
		// TODO Auto-generated method stub
		return violationDescription;
	}
	/**
	 * @return code - codigo de la infracción.
	 */
	public String  getViolationCode() {
		// TODO Auto-generated method stub
		return violationCode;
	}
}
