package test;

import java.util.ArrayList;
import java.util.Iterator;

import junit.framework.TestCase;
import model.data_structures.IteratorLista;
import model.data_structures.LinkedList;


public class pruebasLista extends TestCase
{
	// -----------------------------------------------------------------
	// Atributos
	// -----------------------------------------------------------------

	/**
	 * Es la lista de strings donde se har�n las pruebas
	 */
	private LinkedList<String> listaStrings;
	/**
	 * Es la lista de Integers donde se har�n las pruebas
	 */
	private LinkedList<Integer> listaIntegers;

	// -----------------------------------------------------------------
	// M�todos
	// -----------------------------------------------------------------

	/**
	 * Construye el escenario 1 para probar las listas.
	 */
	private void setupListas1( )
	{
		try
		{
			// Crea la lista de enteros
			listaIntegers=new LinkedList<Integer>();
			//A�ade enteros a la lista de enteros
			listaIntegers.add(1);
			listaIntegers.add(2);
			listaIntegers.add(3);
			listaIntegers.add(4);
			listaIntegers.add(5);
			// Crea la lista de Strings
			listaStrings=new LinkedList<String>();
			//A�ade enteros a la lista de enteros
			listaStrings.add("a");
			listaStrings.add("b");
			listaStrings.add("c");
			listaStrings.add("d");
			listaStrings.add("e");
		}
		catch( Exception e )
		{
			fail( "Error al crear o a�adir elementos de la lista" );
		}
	}

	/**
	 * Construye el escenario 2 para probar las listas.
	 */
	private void setupListas2( )
	{
		// Crea la lista de enteros
		listaIntegers=new LinkedList<Integer>();
		// Crea la lista de Strings
		listaStrings=new LinkedList<String>();
	}


	/**
	 * Prueba si retorna correctamente el tama�o de la lista. <br>
	 * <b> M�todos a probar: </b> <br>
	 * getSize() <br>
	 */
	public void testGetSize( )
	{
		setupListas1();

		assertTrue("Hay errores en el tama�o de la lista",listaIntegers.getSize()==5);
		
		assertTrue("Hay errores en el tama�o de la lista",listaStrings.getSize()==5);
	}

	/**
	 * Prueba si a�ade un elemento al final de la lista <br>
	 * <b> M�todos a probar: </b> <br>
	 * addAtEnd() <br>
	 */
	public void testAddAtEnd( )
	{
		setupListas1();

		listaIntegers.addAtEnd(6);
		assertTrue("Deberia ser el ultimo elemento a�adido", listaIntegers.getCola().getElemento()==6);
		assertTrue("Hay errores en el tama�o de la lista",listaIntegers.getSize()==6);

		listaStrings.addAtEnd("f");
		assertTrue("Deberia ser el ultimo elemento a�adido", listaStrings.getCola().getElemento().equals("f"));
		assertTrue("Hay errores en el tama�o de la lista",listaStrings.getSize()==6);
	}

	/**
	 * Prueba si a�ade un elemento en una posicion dada <br>
	 * <b> M�todos a probar: </b> <br>
	 * addAtk() <br>
	 */
	public void testAddAtk( )
	{
		setupListas1();

		listaIntegers.AddAtK(7,2);

		IteratorLista<Integer> it=listaIntegers.iterator();
		int cont=0;
		int tmp=0;
		boolean loEncontro=false;
		while(it.hasNext()&&!loEncontro)
		{
			cont++;
			tmp=it.next();
			if(tmp==7)
			{
				loEncontro=true;
			}
		}

		assertTrue("Deberia a�adirse en la poscion dada", cont==2);
		assertTrue("Hay errores en el tama�o de la lista",listaIntegers.getSize()==6);


		listaStrings.AddAtK("F",2);

		IteratorLista<String> it2=listaStrings.iterator();
		int cont2=0;
		String tmp2="";
		boolean loEncontro2=false;
		while(it.hasNext()&&!loEncontro2)
		{
			cont2++;
			tmp2=it2.next();
			if(tmp2.equals("F"))
			{
				loEncontro2=true;
			}
		}

		assertTrue("Deberia a�adirse en la poscion dada", cont2==2);
		assertTrue("Hay errores en el tama�o de la lista",listaIntegers.getSize()==6);
	}

	/**
	 * Prueba si el metodo is Empty funciona correctamente <br>
	 * <b> M�todos a probar: </b> <br>
	 * addAtk() <br>
	 */
	public void testIsEmpty()
	{
		setupListas1();
		
		assertFalse(listaStrings.isEmpty());
		assertFalse(listaIntegers.isEmpty());
		
		setupListas2();
		
		assertTrue(listaStrings.isEmpty());
		assertTrue(listaIntegers.isEmpty());
	}
	
	/**
	 * Prueba si el metodo getElement retorna el elemento buscado
	 * <b> M�todos a probar: </b> <br>
	 * getElement() <br>
	 */
    public void testGetElement()
    {
    	setupListas1();
		
    	int busqueda=listaIntegers.getElement(3);
    	
    	assertTrue("El elemento buscado no es retornado", busqueda==3);
    	
    	String busqueda2=listaStrings.getElement("c");
    	
    	assertTrue("El elemento buscado no es retornado", busqueda2.equals("c"));	
    }
    
    /**
	 * Prueba si el metodo delete elimina el elemento requerido
	 * <b> M�todos a probar: </b> <br>
	 * delete() <br>
	 */
    public void testDelete()
    {
    	setupListas1();
		
    	listaIntegers.delete(1);
    	listaIntegers.delete(3);
    	
    	assertNull("No elimino el elemento",listaIntegers.getElement(1));
    	assertNull("No elimino el elemento",listaIntegers.getElement(3));
    	assertTrue("Hay errores en el tama�o de la lista",listaIntegers.getSize()==3);
    	
    	listaStrings.delete("a");
    	listaStrings.delete("c");
    	
    	assertNull("No elimino el elemento",listaStrings.getElement("a"));
    	assertNull("No elimino el elemento",listaStrings.getElement("c"));
    	assertTrue("Hay errores en el tama�o de la lista",listaStrings.getSize()==3);
    	
    }
    
    /**
   	 * Prueba si el metodo deleteAtk elimina el elemento requerido
   	 * <b> M�todos a probar: </b> <br>
   	 * deleteAtk() <br>
   	 */
       public void testDeleteAtk()
       {
       	setupListas1();
   		
       	listaIntegers.delete(3);
       	IteratorLista<Integer> it=listaIntegers.iterator();
       	int tmp=-1;
		int cont=0;
		while(it.hasNext()&&cont!=3)
		{
			tmp=it.next();
			cont++;
		}
		
		assertFalse("No deberia existir el elemento en la posicion",tmp==3);
       	assertNull("No elimino el elemento",listaIntegers.getElement(3));
       	assertTrue("Hay errores en el tama�o de la lista",listaIntegers.getSize()==4);
       	
       	listaStrings.deleteAtk(3);
       	IteratorLista<String> it2=listaStrings.iterator();
       	String tmp2="";
		int cont2=0;
		while(it2.hasNext()&&cont2!=3)
		{
			tmp2=it2.next();
			cont2++;
		}
       
		assertFalse("No deberia existir el elemento en la posicion",tmp2.equals("c"));
       	assertNull("No elimino el elemento",listaStrings.getElement("c"));
       	assertTrue("Hay errores en el tama�o de la lista",listaStrings.getSize()==4);
       	
       }

}
